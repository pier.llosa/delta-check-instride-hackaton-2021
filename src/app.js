var fs = require('fs');
var array1 = fs.readFileSync(process.argv[2]).toString().split("\n");
var array2 = fs.readFileSync(process.argv[3]).toString().split("\n");
var lengthArray;
var countLine = 1;
var errors = 0;

console.log('File 1: ' + process.argv[2] + ' => File 2: ' + process.argv[3]);
console.log('Comparison Started...');
console.log('-------------------------------------------------------------------------------------------------------------');


if (array1.length > array2.length) {
  lengthArray = array2.length;
}

if (array1.length < array2.length) {
  lengthArray = array1.length;
}

if (array1.length === array2.length) {
  lengthArray = array1.length;
}

console.log(lengthArray);

for (i = 0; i < lengthArray; i++) {
  if (array1[i] !== array2[i]) {
    console.log('Error: Line: ' + countLine);
    console.log('File 1: ' + array1[i]);
    console.log('File 2: ' + array2[i]);
    console.log('-------------------------------------------------------------------------------------------------------------');
    errors++;
  }
  countLine++;
}

if (array1.length > array2.length) {
  console.log('Error: File ' + process.argv[2] + ' has more records that ' + process.argv[3]);
  console.log('-------------------------------------------------------------------------------------------------------------');
  errors++;
}

if (array1.length < array2.length) {
  console.log('Error: File ' + process.argv[3] + ' has more records that ' + process.argv[2]);
  console.log('-------------------------------------------------------------------------------------------------------------');
  errors++;
}

console.log('Comparison Completed');
console.log('Error found: ' + errors);
console.log('-------------------------------------------------------------------------------------------------------------');